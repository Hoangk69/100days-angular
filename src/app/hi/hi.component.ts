import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hi',
  templateUrl: './hi.component.html',
  styleUrls: ['./hi.component.css'],
})
export class HiComponent {
  // data
  typeText = 'text';
  user = {
    name: 'Hoang',
    age: 22,
  };

  users: {
    id: number;
    name: string;
  }[] = [
  ];
  // users = []

  constructor() {}
  // aray users

  // envent
  handle() {
    console.log('clicked');
  }
}
